package com.nullin.testrail.sampleproj.pkg;

import com.nullin.testrail.ResultStatus;
import com.nullin.testrail.TestRailListener;
import com.nullin.testrail.TestRailReporter;
import com.nullin.testrail.annotations.TestRailCase;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author nullin
 */
@Listeners(value = TestRailListener.class)
public class TestClassC
{
	
	@BeforeTest
	public void before()
	{
	
	}
	
	
	@TestRailCase("test01")
	@Test
	public void test5()
	{
		Assert.assertEquals(1, 1);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(TestRailReporter.KEY_STATUS, ResultStatus.FAIL);
		//TestRailReporter.getInstance().reportResult("test02", result);
		result.put(TestRailReporter.KEY_STATUS, ResultStatus.PASS);
		result.put(TestRailReporter.KEY_THROWABLE, new IOException("Something very bad happened!!"));
		//TestRailReporter.getInstance().reportResult("test03", result);
	}
}
