package com.nullin.testrail.sampleproj;

import com.nullin.testrail.annotations.TestRailCase;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Properties;

/**
 * @author nullin
 */
public class TestClassB
{
	
	@BeforeTest
	public void before()
	{
		Properties properties = new Properties();
		properties.put("testRail.enabled", true);
		properties.put("testRail.url", "http://google.ru");
		properties.put("testRail.username", "http://google.ru");
		properties.put("testRail.password", "http://google.ru");
		properties.put("testRail.testplan", "http://google.ru");
		System.setProperties(properties);
	}
	
	@TestRailCase(selfReporting = true)
	@Test
	public void test1()
	{
		// do nothing always passes
	}
	
}
